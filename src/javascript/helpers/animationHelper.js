import { createElement } from './domHelper';

export function Animation(position, sound, critical, block) {
  const root = document.getElementById('root');
  const arena = root.childNodes[0];
  if (critical) {
    let criticalImg = createElement({
      tagName: 'img',
      className: `animation___critical-${position}`,
      attributes: {
        src: '../../resources/critical.png',
      },
    });
    arena.append(criticalImg);
    criticalImg.style.opacity = '1';
    let width = 150;
    let start = Date.now();
    sound.play();
    setInterval(function () {
      let timePassed = Date.now() - start;
      width += 1;
      criticalImg.style.opacity = criticalImg.style.opacity - 0.01 + '';
      criticalImg.style.width = width + 'px';
      if (timePassed > 2000) criticalImg.remove();
    }, 20);
  }
  if (block) {
    let blockImg = createElement({
      tagName: 'img',
      className: `animation___block-${position}`,
      attributes: {
        src: '../../resources/block.png',
      },
    });
    arena.append(blockImg);
    blockImg.style.opacity = '1';
    let top = 50;
    let start = Date.now();
    sound.play();
    setInterval(function () {
      let timePassed = Date.now() - start;
      top -= 1;
      blockImg.style.opacity = blockImg.style.opacity - 0.1 + '';
      blockImg.style.top = top + '%';
      if (timePassed > 500) blockImg.remove();
    }, 40);
  }
}
