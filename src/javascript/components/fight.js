import { controls } from '../../constants/controls';
import { Animation } from '../helpers/animationHelper';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const blockSound = createElement({
      tagName: 'audio',
      attributes: {
        src: '../../resources/sounds/block.mp3',
      },
    });
    const attackSound = createElement({
      tagName: 'audio',
      attributes: {
        src: '../../resources/sounds/attack.mp3',
      },
    });
    const criticalSound = createElement({
      tagName: 'audio',
      attributes: {
        src: '../../resources/sounds/critical.mp3',
      },
    });

    const healthPlayerOne = document.getElementById('left-fighter-indicator');
    const healthPlayerTwo = document.getElementById('right-fighter-indicator');
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    const resetTime = 10000;
    let time = new Date().getTime() - resetTime;
    let time2 = new Date().getTime() - resetTime;
    const pressKey = new Map();
    for (let key in controls) {
      if (
        controls[key] === controls.PlayerOneCriticalHitCombination ||
        controls[key] === controls.PlayerTwoCriticalHitCombination
      ) {
        controls[key].forEach((element) => {
          pressKey[element] = false;
        });
      } else {
        pressKey[controls[key]] = false;
      }
    }
    const arrayOfKey = Object.keys(pressKey);
    function handle(event) {
      if (
        arrayOfKey.includes(event.code) &&
        event.code !== controls.PlayerOneAttack &&
        event.code !== controls.PlayerTwoAttack
      ) {
        pressKey[event.code] = true;
      }

      if (event.code === controls.PlayerOneAttack && pressKey[controls.PlayerOneAttack] === true) {
        return;
      }
      if (event.code === controls.PlayerOneAttack && pressKey[controls.PlayerOneBlock] !== true) {
        if (pressKey[controls.PlayerTwoBlock] === true) {
          Animation('right', blockSound, false, true);
          secondFighterHealth -= getDamage(firstFighter, secondFighter, true);
          pressKey[controls.PlayerOneAttack] = true;
        } else if (pressKey[controls.PlayerTwoBlock] === false) {
          attackSound.play();
          secondFighterHealth -= getDamage(firstFighter, secondFighter, false);
          pressKey[controls.PlayerOneAttack] = true;
        }
      }
      if (
        pressKey[controls.PlayerOneCriticalHitCombination[0]] &&
        pressKey[controls.PlayerOneCriticalHitCombination[1]] &&
        pressKey[controls.PlayerOneCriticalHitCombination[2]] &&
        !pressKey[controls.PlayerOneAttack] &&
        !pressKey[controls.PlayerOneBlock]
      ) {
        let BoostTime = new Date().getTime();
        if (BoostTime >= time + resetTime) {
          Animation('left', criticalSound, true, false);
          secondFighterHealth -= firstFighter.attack * 2;
          time = BoostTime;
        }
      }
      if (event.code === controls.PlayerTwoAttack && pressKey[controls.PlayerTwoAttack] === true) {
        return;
      }
      if (event.code === controls.PlayerTwoAttack && pressKey[controls.PlayerTwoBlock] !== true) {
        if (pressKey[controls.PlayerOneBlock] === true) {
          Animation('left', blockSound, false, true);
          firstFighterHealth -= getDamage(secondFighter, firstFighter, true);
          pressKey[controls.PlayerTwoAttack] = true;
        } else if (pressKey[controls.PlayerOneBlock] === false) {
          attackSound.play();
          firstFighterHealth -= getDamage(secondFighter, firstFighter, false);
          pressKey[controls.PlayerTwoAttack] = true;
        }
      }
      if (
        pressKey[controls.PlayerTwoCriticalHitCombination[0]] &&
        pressKey[controls.PlayerTwoCriticalHitCombination[1]] &&
        pressKey[controls.PlayerTwoCriticalHitCombination[2]] &&
        !pressKey[controls.PlayerTwoAttack] &&
        !pressKey[controls.PlayerTwoBlock]
      ) {
        let BoostTime2 = new Date().getTime();
        if (BoostTime2 >= time2 + resetTime) {
          Animation('right', criticalSound, true, false);
          firstFighterHealth -= secondFighter.attack * 2;
          time2 = BoostTime2;
        }
      }
      healthPlayerTwo.style.width = (secondFighterHealth / secondFighter.health) * 100 + '%';
      healthPlayerOne.style.width = (firstFighterHealth / firstFighter.health) * 100 + '%';
      if (secondFighterHealth <= 0 || firstFighterHealth <= 0) {
        firstFighterHealth <= 0 ? (healthPlayerOne.style.width = '0%') : (healthPlayerTwo.style.width = '0%');
        document.removeEventListener('keydown', handle);
        document.removeEventListener('keyup', (event) => {
          if (arrayOfKey.includes(event.code)) {
            pressKey[event.code] = false;
          }
        });

        firstFighterHealth <= 0 ? resolve(secondFighter) : resolve(firstFighter);
      }
    }
    document.addEventListener('keydown', handle);
    document.addEventListener('keyup', (event) => {
      if (arrayOfKey.includes(event.code)) {
        pressKey[event.code] = false;
      }
    });
  });
  // resolve the promise with the winner when fight is over
}

export function getDamage(attacker, defender, isBlocked) {
  if (isBlocked) {
    return 0;
  }
  const damage = getHitPower(attacker);
  const block = getBlockPower(defender);
  return damage - block > 0 ? damage - block : 0;
}

export function getHitPower(attacker) {
  // return hit power
  return attacker.attack * (Math.random() + 1);
}

export function getBlockPower(defender) {
  // return block power
  return defender.defense * (Math.random() + 1);
}
