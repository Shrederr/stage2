import { showModal } from './modal';
import App from '../../app';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  let divWinner = createElement({
    tagName: 'div',
  });
  let imgWinner = createElement({
    tagName: 'img',
    attributes: {
      src: fighter.source,
    },
  });
  divWinner.append(imgWinner);
  function onClose() {
    const root = document.getElementById('root');
    const arena = root.childNodes[0];
    arena.remove();
    divWinner.remove();
    document.querySelector('link').remove();
    new App();
  }
  showModal({ title: 'You are winner' + '  ' + fighter.name, bodyElement: divWinner, onClose });
  // call showModal function
}
