import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if (!fighter) {
    return fighterElement;
  }
  const statesElement = createElement({ tagName: 'div', className: 'preview-states' });
  const nameImg = createElement({ tagName: 'div', className: 'preview-states___name' });
  nameImg.innerText = fighter.name;
  statesElement.append(nameImg);
  const statesElementBlock = createElement({ tagName: 'div', className: 'preview-states___container' });
  statesElement.append(statesElementBlock);
  const statesElementBlockNumbers = createElement({ tagName: 'div', className: 'preview-states___container_numbers' });
  statesElementBlock.append(statesElementBlockNumbers);
  const statesElementBlockImage = createElement({ tagName: 'div', className: 'preview-states___container_images' });
  statesElementBlock.append(statesElementBlockImage);
  const healthImgBlock = createElement({ tagName: 'div', className: 'preview-states___health' });
  const healthImg = createElement({ tagName: 'div' });
  const healthNumber = createElement({ tagName: 'div', className: 'preview-states-health___number' });
  healthImg.style.backgroundImage = "url('../../resources/health.png')";
  fighter.health === 60 ? (healthImg.style.width = '100%') : (healthImg.style.width = '60%');
  healthImg.style.height = '100%';
  healthNumber.innerText = fighter.health;
  statesElementBlockNumbers.append(healthNumber);
  healthImgBlock.append(healthImg);
  const attackImgBlock = createElement({ tagName: 'div', className: 'preview-states___attack' });
  const attackImg = createElement({ tagName: 'div' });
  const attackNumber = createElement({ tagName: 'div', className: 'preview-states-attack___number' });
  attackImg.style.backgroundImage = "url('../../resources/attack.png')";
  attackImg.style.width = `${fighter.attack * 20 + '%'}`;
  attackImg.style.height = '100%';
  attackNumber.innerText = fighter.attack;
  statesElementBlockNumbers.append(attackNumber);
  attackImgBlock.append(attackImg);
  const defenseImgBlock = createElement({ tagName: 'div', className: 'preview-states___defense' });
  const defenseImg = createElement({ tagName: 'div' });
  const defendNumber = createElement({ tagName: 'div', className: 'preview-states-defend___number' });
  defenseImg.style.backgroundImage = "url('../../resources/defense.png')";
  defenseImg.style.width = `${fighter.defense * 20 + '%'}`;
  defenseImg.style.height = '100%';
  defendNumber.innerText = fighter.defense;
  statesElementBlockNumbers.append(defendNumber);
  defenseImgBlock.append(defenseImg);
  statesElementBlockImage.append(healthImgBlock);
  statesElementBlockImage.append(attackImgBlock);
  statesElementBlockImage.append(defenseImgBlock);
  fighterElement.append(createFighterImage(fighter));
  fighterElement.append(statesElement);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  return createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });
}
